// alert("Hello, Batch 241!");

// Arithmetic Operators

let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;

console.log("Result of addition operator: " + sum);

// Subtration Operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multipliation Operator
let product = x * y;
console.log("Result of the multiplication operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of the division operator: " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of the modulo operator: " + remainder);

// Assignment Operator
// Basic Assignment Operator(=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;

// Addition Assignment Operator
// Uses the current value of the variable and it ADDS a number (2) to itself. Afterwards, reassigns it as a new value.
// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division (-=, *=, /=)

// Subtration Assignment Operator
assignmentNumber -= 2; // assignmentNumber = assignmentNumber - 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber);

// Multiplication Assignment Operator
assignmentNumber *= 2; // assignmentNumber = assignmentNumber * 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber);

// Division Assignment Operator
assignmentNumber /= 2; // assignmentNumber = assignmentNumber / 2;
console.log("Result of the division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
*/

// The order of operations can be changed by adding parenthesis to the logi
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas operation: " + pemdas);

let sample = 5 * 2 % 10 + 1; //1
console.log(sample);

// INCREMENT and DECREMENT
// Operators that dd or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// Pre-Increment
// let preIncrement = ++z;
// console.log("Result of the pre-increment: " + preIncrement);

// console.log("Result of pre-increment: " + z);

// Post-Increment
// The value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the postIncrement: " + z);

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1

	Pre-increment - subtracts 1 first before reading value
	Post-increment - reads value first before subtracting 1
*/


let a = 2;

// Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement"
// let preDecrement = --a;
// console.log("Result of the pre-decrement: " + preDecrement);
// console.log("Result of the pre-decrement: " + a);

// Post-Decrement
// The value of "a" is returned and stored in the variable "postDecrement" then the value of "a" is increased by one when called.
let postDecrement = a--;
console.log("Result of the postDecrement: " + postDecrement);
console.log("Result of the post decrement: " + a);


// TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another

*/

let numA = '10'; //string
let numB = 12; //number

/*
	Adding/concatenating a string and a number will result as a string

*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of a Number and Boolean

/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);


// Comparison Operator
let juan = 'juan';

// Equality Operator(==)
/*

	-Checks whether the operands are equal/have the same content
	-Attempts to CONVERT and COMPARE operands of different data types
	-Returns a boolean value (true / false)

*/

console.log(1 == 1); //True
console.log(1 == 2); //False
console.log(1 == '1'); //True
console.log(1 == true); //True

// compare two strings that are the same
console.log('juan' == 'juan'); //True
console.log('true' == true); //False

// Inequality Operator(!=)
/*
	-Checks whether the operands are not equal/have different content
	-Attempts to CONVERT and COMPARE operands of diff data types

*/
console.log(1 != 1); //F
console.log(1 != 2); //T
console.log(1 != '1'); //F
console.log(1 != true); //F
console.log('juan' != 'juan'); //F
console.log('juan' != juan); //F

// Strict Equality Operator(===)
/*

	-Check whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values

*/

console.log(1 === 1); //T
console.log(1 === 2); //F
console.log(1 === '1'); //F
console.log(1 === true); //F
console.log('juan' === 'juan'); //T
console.log(juan === 'juan'); //T

// Strict Inequality Operator (!==)
/*
	-Check whether the operands are not equal/have the same content
	-Also COMPARES the data types of 2 values

*/

console.log(1 !== 1); //F
console.log(1 !== 2); //T
console.log(1 !== '1'); //T
console.log(1 !== true); //T
console.log('juan' !== 'juan'); //F
console.log(juan !== 'juan'); //F


// Relational Operator
// Returns a boolean value
let j = 50;
let k = 65;

// Greater than Operator (>)
let isGreaterThan = j > k;
console.log (isGreaterThan);

// Less than Operator (<)
let isLessThan = j < k;
console.log (isLessThan);

// Greater than or Equal operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual);

// Less than or Equal operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual);

let numStr = "30";
console.log (j > numStr);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&& - Double Ampersand)
// Returns true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

// Logical OR operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

// Logical NOT Operator (! - Exclamation point)
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);